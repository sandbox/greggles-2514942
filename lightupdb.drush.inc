<?php

/**
 * @file
 * Drush-related callbacks for my project.
 */

/**
 * Implements hook_drush_command().
 */
function lightupdb_drush_command() {
  $items['lightupdb-updb'] = array(
    'description' => "A copy of `drush updb` without a cache clear.",
    'callback' => 'drush_lightupdb_updb',
    'aliases' => array('lightupdb'),
  );
  return $items;
}

/**
 * Drush callback.
 *
 * A copy of `drush updb` without the cache clear.
 */
function drush_lightupdb_updb() {
  if (drush_get_context('DRUSH_SIMULATE')) {
    drush_log(dt('updatedb command does not support --simulate option.'), 'ok');
    return TRUE;
  }

  drush_include_engine('drupal', 'update', drush_drupal_major_version());
  if (update_main() === FALSE) {
    return FALSE;
  }

  drush_log(dt('Finished performing updates.'), 'ok');
}
